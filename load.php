<?php

require_once("Config/db.php");

$dbconnection = mysqli_connect($hostname,$username,$password,$database);

if (!$dbconnection) {
    die("Connection failed: " . mysqli_connect_error());
}

$data = array();
$query = "SELECT * FROM events ORDER BY id";

$result = mysqli_query($dbconnection, $query);
if(mysqli_num_rows($result)>0){

	while ($row = mysqli_fetch_assoc($result)){
		$data[] = array(
			  'id'   => $row["id"],
			  'title'   => $row["title"],
			  'description' => $row["description"],
			  'type' => $row["type"],
			  'start'   => $row["start_event"],
			  'end'   => $row["end_event"]
			 );
	}
}

echo json_encode($data);