-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Mar 2018 la 14:04
-- Versiune server: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `calendar`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1=Normal, 0= AllDay',
  `start_event` datetime NOT NULL,
  `end_event` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Salvarea datelor din tabel `events`
--

INSERT INTO `events` (`id`, `title`, `description`, `type`, `start_event`, `end_event`) VALUES
(1, 'titlu nou', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'fsadgsad', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'abcd', '', 1, '1990-00-00 00:00:00', '2010-00-00 00:00:00'),
(17, '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'dat', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'fds', '', 1, '2018-02-26 08:00:00', '2018-02-26 08:30:00'),
(105, 'dfdsafads', '', 1, '2018-02-26 09:00:00', '2018-02-26 11:00:00'),
(118, 'a', '', 1, '2018-02-28 09:00:00', '2018-02-28 11:00:00'),
(123, 'a', '', 1, '2018-03-07 07:00:00', '2018-03-07 09:00:00'),
(124, 'a', '', 1, '2018-03-08 09:00:00', '2018-03-08 11:00:00'),
(125, 'czxcxz', '', 1, '2018-03-08 09:00:00', '2018-03-08 11:00:00'),
(127, '333', '', 1, '2018-03-06 09:30:00', '2018-03-06 11:00:00'),
(129, 'dfdsgfd', '', 1, '2018-03-10 09:00:00', '2018-03-10 11:00:00'),
(130, 'dsfasdf', '', 1, '2018-03-08 09:00:00', '2018-03-08 11:00:00'),
(132, 'fdsa', '', 1, '2018-03-04 11:00:00', '2018-03-04 12:00:00'),
(133, 'fdsaf', '', 1, '2018-03-05 02:00:00', '2018-03-05 03:00:00'),
(139, 'fdasfvsd', 'vsa', 1, '2018-02-25 09:00:00', '2018-02-25 11:00:00'),
(147, 'NEW', 'NEW', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 'NEW', 'NEW', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 'nou', 'nou', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 'NEW', 'NEW', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 'NOU', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 'yes', 'please', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 'NOU', 'NPU', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 'mamama', 'mamamama', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 'yes', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 'xxxxx', 'xxxxx', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 'imediat', '', 1, '2018-03-09 12:00:00', '2018-03-09 13:30:00'),
(164, 'fdsfsd', '', 1, '2018-03-02 08:30:00', '2018-03-02 10:00:00'),
(165, 'fdsfsd', '', 1, '2018-03-09 07:00:00', '2018-03-09 08:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
