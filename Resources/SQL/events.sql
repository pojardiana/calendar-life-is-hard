CREATE DATABASE calendar;


CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  `type` INT(11) DEFAULT 1 COMMENT '1=Normal, 0= AllDay' NOT NULL,
  `start_event` datetime NOT NULL,
  `end_event` datetime NOT NULL
) 