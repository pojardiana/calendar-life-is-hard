
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Event Calendar</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>

    
    <script>
        $(document).ready(function() {

            var calendar = $('#calendar').fullCalendar({

                editable:true,
                header:{
                    left:'prevYear, prev, next, nextYear, today',
                    center:'title',
                    right:'month, agendaWeek, agendaDay'
                },
                events:'load.php',
                   
                selectable:true,
                selectHelper:true,
                
                slotDuration:  '00:30:00',
                snapDuration:  '01:00:00',
                displayEventEnd: true,
                
               

               select:  function(start, end,jsEvent,view){
                    if(view.name=='agendaDay'||view.name=='agendaWeek'){
                         
                        var title = prompt("Add event title");
                        var description = prompt("Add a description - optional");
                    
                        
                      if(title) {
                            var start_event = $.fullCalendar.formatDate(start, "YYYY-MM-DD HH:mm:ss");
                            var end_event = $.fullCalendar.formatDate(end, "YYYY-MM-DD HH:mm:ss");
                      
                            $.ajax({
                                
                                url:"insert.php",
                                type:"POST",
                                data:{title:title, description:description, start_event:start_event, end_event:end_event},
                                success:function(){
                                    calendar.fullCalendar('refetchEvents');
                                    alert("Successfully added!");
                                },
                                error:function(){
                                    alert("Event NOT added!");

                                }

                            })
                        }
                    }
                }, 

                
                // editable:true,

                dayClick:function(date, jsEvent, view){
                    if(view.name=='month'){
                        var title = prompt("Add day event title");
                        var description = prompt("Add a description - optional");
                     
                      
                        if(title) {
                            var start_event = $.fullCalendar.formatDate(date, "YYYY-MM-DD 09:00:00");
                            var end_event = $.fullCalendar.formatDate(date, "YYYY-MM-DD 10:00:00");
                            $.ajax({
                                url:"insert.php",
                                type:"POST",
                                data:{title:title, description:description, start_event:start_event, end_event:end_event},
                                success:function(){
                                    calendar.fullCalendar('refetchEvents');
                                    alert("Successfully added!");
                                },
                                error:function(){
                                    alert("Event NOT added!");
                                }

                            })
                        }
                    }
                },

                eventResize:function(event){
                    var start_event = $.fullCalendar.formatDate(event.start, "YYYY-MM-DD HH:mm:ss");
                     var end_event = $.fullCalendar.formatDate(event.end, "YYYY-MM-DD HH:mm:ss");
                     var title = event.title;
                     var id = event.id;
                    $.ajax({
                        url:"update.php",
                        type:"POST",
                        data:{title:title, start_event:start_event, end_event:end_event, id:id},
                        success:function(){
                            calendar.fullCalendar('refetchEvents');
                            alert('Event Updated!');
                        },
                        error:function(){
                            alert("Event NOT updated!");
                        }
                    })
                },

                eventDrop:function(event) {
                var start_event = $.fullCalendar.formatDate(event.start, "YYYY-MM-DD HH:mm:ss");
                     var end_event = $.fullCalendar.formatDate(event.end, "YYYY-MM-DD HH:mm:ss");
                     var title = event.title;
                     var id = event.id;
                    $.ajax({
                        url:"update.php",
                        type:"POST",
                        data:{title:title, start_event:start_event, end_event:end_event, id:id},
                        success:function(){
                            calendar.fullCalendar('refetchEvents');
                            alert('Event Updated!');
                        },
                        error:function(){
                            alert("Event NOT updated!");
                        }
                    })
                },

               

                eventClick:function(event){
                    if(confirm("Do you want to CHANGE this event?")) {
                        var title = prompt("Add day event title");
                        var description = prompt("Add a description - optional");
                        var id = event.id;
                      
                        if(title) {
                            
                            $.ajax({
                                url:"updateclick.php",
                                type:"POST",
                                data:{title:title, description:description,id:id},
                                success:function(){
                                    calendar.fullCalendar('refetchEvents');
                                    alert("Successfully updated!");
                                },
                                error:function(){
                                    alert("Event NOT updated!");
                                }

                            })
                        }                
                    } else {
                        if (confirm("Do you want to DELETE this event?")){
                            var id = event.id;
                            $.ajax({
                                url:"delete.php",
                                type:"POST",
                                data:{id:id},
                                success:function(){
                                    calendar.fullCalendar('refetchEvents');
                                    alert("Event Removed");
                                },
                                error:function(){
                                    alert("Event NOT deleted!");
                                }
                            })

                        }
                    }
                },


            });

        });

    </script>

</head>
<body>

    <h2 align="center">  Calendar </h2>
    <div class="container">
    <div id="calendar">
    </div>      
    </div>
</body>
</html>